﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	public Transform[] spawnPositions;
	public GameObject enemyPrefab;
	public float spawnCooldown;
	private GameObject objRef;
	private float cd = 0;

	void Update () {
		if (cd > 0)
			cd -= Time.deltaTime;

		if (cd <= 0 || objRef == null) {
			if (objRef != null)
				Destroy (objRef);
			Vector3 spawnPos = spawnPositions [Random.Range (0, spawnPositions.Length)].position;
			objRef = Instantiate (enemyPrefab, spawnPos, Quaternion.identity);
			cd = spawnCooldown;
		}
	}
}
