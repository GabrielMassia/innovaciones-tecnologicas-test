﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimAndShot : MonoBehaviour {

	public GameObject bullet;
	public float bulletSpeed;
	public float cooldown;
	private float cd = 0;

	void Update () {
		if (cd > 0)
			cd -= Time.deltaTime;

		if (!Application.isMobilePlatform) {
			if (Input.GetKeyDown (KeyCode.Mouse0) && cd <= 0) {
				Vector3 tgt = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				Vector3 target = new Vector3 (tgt.x, 0.5f, tgt.z);
				GameObject bulletRef = Instantiate (bullet, transform.position, Quaternion.identity);
				Rigidbody rbRef = bulletRef.GetComponent<Rigidbody> ();
				bulletRef.transform.LookAt (target);
				rbRef.AddRelativeForce (Vector3.forward * bulletSpeed, ForceMode.Impulse);
				cd = cooldown;
			}
		} 
		else {
			if (Input.touchCount > 0) {
				Vector3 tgt = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);
				Vector3 target = new Vector3 (tgt.x, 0.5f, tgt.z);
				GameObject bulletRef = Instantiate (bullet, transform.position, Quaternion.identity);
				Rigidbody rbRef = bulletRef.GetComponent<Rigidbody> ();
				bulletRef.transform.LookAt (target);
				rbRef.AddRelativeForce (Vector3.forward * bulletSpeed, ForceMode.Impulse);
				cd = cooldown;
			}
		}

		if (!Application.isMobilePlatform && Input.GetKeyDown (KeyCode.Escape))
			Application.Quit ();
	}
}