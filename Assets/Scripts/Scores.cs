﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scores : MonoBehaviour {

	public static int score = 0;
	private Text textRef;

	void Update () {
		textRef = GetComponent<Text> ();
		if (textRef) {
			textRef.text = "Score: " + score;
		}
	}
}