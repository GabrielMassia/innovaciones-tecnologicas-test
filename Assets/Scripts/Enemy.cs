﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	public int scoreAwarded;

	void OnCollisionEnter (Collision c) {
		if (c.gameObject.tag == "Bullet") {
			Scores.score += scoreAwarded;
			Destroy (c.gameObject);
			Destroy (gameObject);
		}
	}
}
